<?php

namespace App\Controller;

use App\Entity\Student;
use App\Form\StudentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 *  CRUD Student
 * @package App\Controller
 * @Route("/etudiant")
 */
class StudentController extends Controller
{
    /**
     * show all students
     * @Route("/", name="home")
     * @return Response
     */
    public function index(): Response
    {
        $students = $this->getDoctrine()->getRepository(Student::class)->findAll();

        return $this->render("student/index.html.twig", [
            'students' => $students
        ]);
    }

    /**
     * show 1 student in id
     * @Route("/show/{id}", name="show-student", methods="GET", requirements={"id"="\d+"})
     * @param int $id
     * @return  Response
     */
    public function showStudent(int $id): Response
    {
        $student = $this->getDoctrine()->getRepository(Student::class)->find($id);

        return $this->render('student/show-student.html.twig', ['student' => $student]);
    }

    /**
     * @Route("/ajouter-un-etudiant", name="add-student")
     * @param Request $request
     * @return Response
     */
    public function addStudent(Request $request): Response
    {
        $student = new Student();
        $form = $this->createForm(StudentType::class, $student);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($student);
            $em->flush();
            $this->addFlash('success', 'L\'étudiant a bien été ajouter');

            return $this->redirectToRoute('home');
        }

        return $this->render("student/add-student.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edtier-un-etudiant/{id}", name="edit-student", requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editStudent(Request $request, int $id): Response
    {
        $student = $this->getDoctrine()->getRepository(Student::class)->find($id);
        $form = $this->createForm(StudentType::class, $student);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($student);
            $em->flush();
            $this->addFlash('success', 'L\'étudiant a bien été modifier');
            return $this->redirectToRoute('show-student', ['id' => $student->getId()]);
        }
        return $this->render("student/edit-student.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="delete-student", requirements={"id"="\d+"})
     * @param int $id
     * @return Response
     */
    public function deleteStudent(int $id): Response
    {
        $student = $this->getDoctrine()->getRepository(Student::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($student);
        $em->flush();
        $this->addFlash('delete', 'L\'étudiant à bien étais supprimé !');
        return $this->redirectToRoute('home');
    }
}