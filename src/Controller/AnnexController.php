<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
// Front controller this controller manages all the party front of the website
class AnnexController extends Controller
{
    /**
     * @Route("/", name="redirect")
     * @return RedirectResponse
     */
    public function redirectIndex(): RedirectResponse
    {
        return $this->redirect($this->generateUrl('home'));
    }
}