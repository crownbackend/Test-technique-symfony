<?php

namespace App\Controller;

use App\Entity\Student;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Api Students return jsonResponse
 * @package App\Controller
 * @Route("/api")
 */
class ApiStudentController extends Controller
{
    /**
     * @Route("/students", methods="GET")
     * @return JsonResponse
     * @SWG\Response(
     *     response="200",
     *     description="Returns all students in database"
     * )
     * @SWG\Tag(name="Students")
     */
    public function getStudents(): JsonResponse
    {
        $students = $this->getDoctrine()->getRepository(Student::class)->findAll();
        /**
         * @var $students Student[]
         */

        $formatted = [];
        foreach ($students as $student) {
            $formatted[] = [
                'id' => $student->getId(),
                'lastName' => $student->getLastName(),
                'firstName' => $student->getFirstName(),
                'department' => [
                    'id' => $student->getDepartment()->getId(),
                    'name' => $student->getDepartment()->getName(),
                    'capacity' => $student->getDepartment()->getCapacity()
                ]
            ];
        }

        return new JsonResponse($formatted);
    }

}