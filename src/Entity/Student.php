<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StudentRepository")
 * @UniqueEntity("numEtud", message="Le numéro étudiant est déjà utilisé")
 */
class Student
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank(message="Le prénom ne doit pas être vide")
     * @Assert\Length(min="2", max="25",
     *     minMessage="le prénom doit contenir au minimum {{ limit }} caractères",
     *     maxMessage="le prénom ne doit pas dépassé {{ limit }} caractères"
     * )
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank(message="Le nom de famille ne doit pas être vide")
     * @Assert\Length(min="2", max="25",
     *     minMessage="le nom de famille doit contenir au minimum {{ limit }} caractères",
     *     maxMessage="le nom de famille ne doit pas dépassé {{ limit }} caractères"
     * )
     */
    private $lastName;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     * @Assert\NotBlank(message="Le numéro d'étudiant ne doit pas être vide")
     * @Assert\Length(min="5", max="10",
     *     minMessage="le numéro d'étudiant doit contenir au minimum {{ limit }} caractères",
     *     maxMessage="le numéro d'étudiant ne doit pas dépassé {{ limit }} caractères"
     * )
     */
    private $numEtud;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Department", inversedBy="students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $department;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getNumEtud(): ?int
    {
        return $this->numEtud;
    }

    public function setNumEtud(int $numEtud): self
    {
        $this->numEtud = $numEtud;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }
}
